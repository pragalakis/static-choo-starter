const app = require('./choo.js');

app.route('/', require('./src/pages/index.js'));
app.route('/page-2', require('./src/pages/page-2.js'));

// fallback
app.route('/404', require('./src/pages/404.js'));
app.route('/*', require('./src/pages/404.js'));

app.mount('body');
