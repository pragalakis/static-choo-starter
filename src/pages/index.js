const html = require('choo/html');
const app = require('../../choo.js');
const css = require('sheetify');

app.use(countStore);

function countStore(state, emitter) {
  state.count = 0;
  emitter.on('increment', (count) => {
    state.count += count;
    emitter.emit('render');
  });
}

module.exports = (state, emit) => {
  emit('DOMTitleChange', "My app's title");

  const onclick = () => {
    emit('increment', 1);
  };

  return html`
    <body class=${prefix}>
      <h1>The count is: ${state.count}</h1>
      <button onclick=${onclick}>Increment</button>
      <a href="/page-2" class="link">Go to page 2</a>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
    display: flex;
    height: 100vh;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }

  :host h1 {
    color: #f77;
    font-size: 2.5rem;
  }

  :host button {
    background-color: #f77;
    padding: 0.625rem 0.9375rem;
    border: none;
    font-weight: 700;
    font-size: 0.9rem;
    color: #ffe5e5;
    letter-spacing: 0.125rem;
    outline: none;
  }

  :host .link {
    margin-top: 2rem;
  }
`;
