const html = require('choo/html');
const css = require('sheetify');

module.exports = (state, emit) => {
  emit('DOMTitleChange', '404');

  return html`
    <body class=${prefix}>
      <header>
        <h1>404 - Page Not Found</h1>
      </header>
      <main>
        <a href='/'>Go back to homepage</a>
      </main>
    </body>
  `;
};

const prefix = css`
  :host {
    font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', Roboto,
      Oxygen-Sans, Ubuntu, Cantarell, 'Helvetica Neue', sans-serif;
    display: flex;
    height: 100vh;
    background-color: #ffe5e5;
    font-size: 16px;
    margin: 0;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;
