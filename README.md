# static-choo-starter

This repo provides a quick way to start building simple static sites with choo.

[https://pragalakis.gitlab.io/static-choo-starter/](https://pragalakis.gitlab.io/static-choo-starter/)

## What's choo?

[Choo](https://choo.io/) is a 4kb framework for creating sturdy frontend applications which is super minimal and easy to begin.

## What's inside?

- Choo - a frontend framework
- Browserify - a bundler
- Budo - a dev server
- Sheetify - a css bundler
- Indexhtmlify - wraps a js bundle in the minimum html to be browser runnable

## How to start?

- Clone this repo
- Remove the `.git` folder
- Install all the dependencies: `npm install`
- Run the site locally: `npm start`
- Build the site: `npm run build`. This will generate an `index.html`.

Start creating!
